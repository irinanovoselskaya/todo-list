(function() {

var listTask, services;

createList();

function Task (name, descr, date) {
    this.id = date.getTime();
    this.name = name;
    this.date = date.toDateString() + ', ' + date.toTimeString().slice(0, 5);
    this.descr = descr;
    this.status = 'Complete';
}

Task.prototype.editTask = function(pos) {
    var taskName, taskDescr;

    taskName = listTask[pos].name;
    taskDescr = listTask[pos].descr;

    document.getElementById('currPos').value = pos;
    document.getElementById('nameTask').value = taskName;
    document.getElementById('descrTask').value = taskDescr;
    document.getElementById('btnEditTask').className = 'btn btn-danger';

    actionWindow('edit');
};

Task.prototype.changeStatus = function(pos) {
    var taskStatus;

    taskStatus = listTask[pos].status;

    if (taskStatus === 'Activate') {
        listTask[pos].status = 'Complete';
    } else {
        listTask[pos].status = 'Activate';   
    }

    setStorage();
    createList();
};

Task.prototype.deleteTask = function (pos) {
    listTask.splice(pos, 1);
    setStorage();
    createList();
}

var btnpopup = document.getElementById('addTask');
btnpopup.addEventListener('click', function(){actionWindow('add')}, false);

var btnAddTask = document.getElementById('formAddTask');
btnAddTask.addEventListener('submit', formTask, false);

var btnCancel = document.getElementById('btnCancel');
btnCancel.addEventListener('click', function(){actionWindow('close')}, false);

var btnClearList = document.getElementById('btnClearList');
btnClearList.addEventListener('click', clearList, false);

var btnActionTasks = document.getElementsByClassName('action');
Array.prototype.forEach.call(btnActionTasks, function(btnActionTask){
    btnActionTask.addEventListener('click', actionTask, false);
});

var btnSortDate = document.getElementById('btnDate');
btnSortDate.addEventListener('click', sortDate, false);

var btnSortName = document.getElementById('btnName');
btnSortName.addEventListener('click', sortName, false);

function actionWindow (taskAction) {
    var popup, overlay;

    popup = document.getElementById('windowAddTask'),
    overlay = document.getElementById('overlay');

    if (taskAction === 'close') {
        popup.style.display = 'none';
        overlay.style.display = 'none';

        document.getElementById('nameTask').value = '';
        document.getElementById('descrTask').value = '';
        document.getElementById('btnAddTask').className = 'btn btn-danger hidden';
        document.getElementById('btnEditTask').className = 'btn btn-danger hidden';
    } else {
        popup.style.display = 'block';
        overlay.style.display = 'block';

        if (taskAction === 'add') {
            document.getElementById('btnAddTask').className = 'btn btn-danger';
        }
    }
}

function actionTask () {
    var taskId, task, action, pos;

    action = this.getAttribute('data-action');
    taskId = this.parentNode.nextSibling.value;

    for (pos = 0; pos < listTask.length; pos++) {
        if (listTask[pos].id === +taskId) {
            task = listTask[pos];
            //task = new Task(listTask[pos].name, listTask[pos].descr, listTask[pos].date);
            break;
        }
    }

    task.prototype = Object.create(Task.prototype);

    switch(action) {
        case 'edit':
            task.prototype.editTask(pos);
            break;

        case 'changeStatus':
            task.prototype.changeStatus(pos);
            break;
        
        case 'delete':
            task.prototype.deleteTask(pos);            
            break;
        
        default:
            return false;
    }
}

function createList () {
    var listHtml, itemHtml, itemName, itemDescr, itemId, itemData, itemStatus;

    listTask = JSON.parse(localStorage.getItem('listTask')) || [];
    getStorageSort();

    if (services.orderBy) {
        if (services.orderBy === 'date') {
            listTask.sort(function(task1, task2){
                if (services.order === 'ASC') {
                    return task2.dateSort - task1.dateSort;    
                } else {
                    return task1.dateSort - task2.dateSort;
                }
                
            });
        } else {
            listTask.sort(function(task1, task2){
                if (services.order === 'ASC') {
                    if (task1.name > task2.name) return 1;
                    if (task1.name < task2.name) return -1;    
                } else {
                    if (task2.name > task1.name) return 1;
                    if (task2.name < task1.name) return -1;
                }
                
            });
        }
    }

    listHtml = document.createElement('div');

    if (listTask.length > 0) {

        for (var i = 0; i < listTask.length; i++) {

            itemName = listTask[i].name;
            itemDescr = listTask[i].descr;
            itemId = listTask[i].id;
            itemData = listTask[i].date;
            itemStatus = listTask[i].status;

            itemHtml = document.createElement('div');
            itemHtml.setAttribute('class', 'item');
            itemHtml.innerHTML = '<h3 class="text-center">' +
                itemName + '<br><small>' + itemData + '</small></h3>'+
                '<p class="descr">' + itemDescr + '</p>'+
                '<div class="btn-action">' +
                '<a href="#" class="btn btn-sm btn-info action" data-action="edit">Edit</a> ' +
                '<a href="#" class="btn btn-sm btn-success action" data-action="changeStatus" data-status="' + itemStatus + '">' +  itemStatus + '</a> '+
                '<a href="#" class="btn btn-sm btn-danger action" data-action="delete">Delete</a></div>' +
                '<input type="hidden" name="id_task" value="' + itemId + '"/>';

            listHtml.appendChild(itemHtml);
        }

        document.getElementById('content').innerHTML = '';
        document.getElementById('content').appendChild(listHtml);

    } else {

        document.getElementById('content').innerHTML = '';

    }

    var btnActionTasks = document.getElementsByClassName('action');
    Array.prototype.forEach.call(btnActionTasks, function(btnActionTask){
        btnActionTask.addEventListener('click', actionTask, false);
    });

    hideServices();
}

function hideServices () {
    var listLength, btnServices;

    addDataSort();

    listLength = listTask.length;
    btnServices = document.getElementsByClassName('btn-services');

    Array.prototype.forEach.call(btnServices, function(btnService){
        if (listLength < 1) {
            btnService.setAttribute('disabled', 'disabled');
        } else {
            btnService.removeAttribute('disabled');
        }
    });
}

function formTask () {
    var nameTask, descrTask, btnStyle;

    nameTask = document.getElementById('nameTask').value || 'New Task';
    descrTask = document.getElementById('descrTask').value || '';
    btnStyle = getComputedStyle(btnEditTask);

    if (btnStyle.display === 'none') {
        var task, dateTask;

        dateTask = new Date();
        task = new Task(nameTask, descrTask, dateTask);
        listTask.push(task);
    } else {
        var pos;

        pos = document.getElementById('currPos').value;

        listTask[pos].name = nameTask;
        listTask[pos].descr = descrTask;
    }

    actionWindow('close');
    setStorage();
    createList();
}

function clearList () {
    var answ = confirm('Are you sure?');
    if (answ) {
        listTask = [];
        services = {};
        setStorage();
        createList();
    }
}

function setStorage () {
    localStorage.setItem('listTask', JSON.stringify(listTask));
    localStorage.setItem('services', JSON.stringify(services));
}

function getStorageSort () {
    services = JSON.parse(localStorage.getItem('services')) || {};
    services.orderBy = services.orderBy || 'date';
    services.order = services.order || 'ASC';
}

function sortDate () {
    listTask = JSON.parse(localStorage.getItem('listTask')) || [];
    getStorageSort();

    if (services.counterSort) {
        services.order = 'DESC';
        services.counterSort = 0;
    } else {
        services.order = 'ASC';
        services.counterSort = 1;
    }

    listTask.sort(function(task1, task2){
        if (services.order === 'DESC') {
            return task2.id - task1.id;    
        } else {
            return task1.id - task2.id;
        }
    });

    services.orderBy = 'date';

    setStorage();
    createList();
    addDataSort();
}

function sortName () {
    listTask = JSON.parse(localStorage.getItem('listTask')) || [];
    getStorageSort();

    if (services.counterSort) {
        services.order = 'DESC';
        services.counterSort = 0;
    } else {
        services.order = 'ASC';
        services.counterSort = 1;
    }

    listTask.sort(function(task1, task2){
        if (services.order === 'ASC') {
            if (task1.name > task2.name) return 1;
            if (task1.name < task2.name) return -1;    
        } else {
            if (task2.name > task1.name) return 1;
            if (task2.name < task1.name) return -1;
        }
    });

    services.orderBy = 'name';

    setStorage();
    createList();
    addDataSort();
}

function addDataSort () {
    var btnSortDate = document.getElementById('btnDate');
    var btnSortName = document.getElementById('btnName');

    if (services.orderBy === 'date') {
        btnSortDate.setAttribute('data-orderby', 'active');
        btnSortDate.setAttribute('data-order', services.order);

        btnSortName.removeAttribute('data-orderby');
        btnSortName.removeAttribute('data-order');
    } else {
        btnSortName.setAttribute('data-orderby', 'active');
        btnSortName.setAttribute('data-order', services.order);
        
        btnSortDate.removeAttribute('data-orderby');
        btnSortDate.removeAttribute('data-order');
    }
}

})();